package com.company;


import java.io.FileNotFoundException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.TreeMap;



public class Main {

    TreeMap<String, Account> accounts = new TreeMap<>();

    Main(){
        init();
    }

    private void init(){

        BufferedReader br = null;
        String line;

        try{
            br = new BufferedReader(new FileReader("rawData.csv"));
        } catch (FileNotFoundException ex){
            System.out.println(ex.getMessage() + " The File was not found ");
            System.exit(0);  //close out the system when the file is not found
        }

        try {
            while ((line = br.readLine()) != null) {
                String[] tmp = line.split(",");
                if(!accounts.containsKey(tmp[0])  && !tmp[0].equalsIgnoreCase("accountNumber")) {
                    //tmp[0] //account number
                    //tmp[1] //account type
                    //tmp[2] ///givenName
                    //tmp[3] //familyName
                    accounts.put(tmp[0], new Account(tmp[0], tmp[1], tmp[2], tmp[3]));
                }

            }

        } catch(IOException ex){
            System.out.println(ex.getMessage() + " Error reading file");
        }  finally {



            for(String a : accounts.keySet()){
                System.out.println(a);
            }
        }


    }



    public static void main(String[] args) {

        new Main();

    }
}
