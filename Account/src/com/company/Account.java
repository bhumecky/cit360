package com.company;

public class Account {

    private String accountNumber;
    private String accountType;
    private String givenName;
    private String familyName;

    Account(String accountNumber, String accountType, String givenName, String familyName){

        this.accountNumber = accountNumber;
        this.accountType = accountType;
        this.givenName = givenName;
        this.familyName = familyName;

    }

    private void setAccoutNumber(String a){
        this.accountNumber = a;
    }

    private void setAccountType(String at){
        this.accountType = at;
    }

    private void setGivenName(String gn){
        this.givenName = gn;
    }

    private void setFamilyName(String fn){
        this.familyName = fn;
    }

    private String getAccountNumber(){
        return this.accountNumber;
    }

    private String getAccountType(){
        return this.accountType;
    }

    private String getGivenName(){
        return this.givenName;
    }

    private String getFamilyName(){
        return this.familyName;
    }


}
