package com.company;

import static org.junit.Assert.*;
import com.company.MammalBean;
import com.company.DogBean;
import org.junit.Test;

import java.util.TreeMap;


public class MammalBeanTest {




    @Test
    public void mammalBeanTest1(){

            //step 1
            double x = 0.001;


            //mbeans
            MammalBean m1 = new MammalBean(3, "green", 14);
            MammalBean m2 = new MammalBean(4, "red", 17);
            MammalBean m3 = new MammalBean(2, "brown", 4);

            assertEquals(3, m1.getLegCount(), x);
            assertEquals("green", m1.getColor());
            assertEquals(14, m1.getHeight(), x);

            assertEquals(4, m2.getLegCount(), x);
            assertEquals("red", m2.getColor());
            assertEquals(17, m2.getHeight(), x);

            assertEquals(2, m3.getLegCount(), x);
            assertEquals("brown", m3.getColor());
            assertEquals(4, m3.getHeight(), x);


            //dbeann

            DogBean d1 = new DogBean("Dog", "Dog 1", 4, "purple", 13);
            DogBean d2 = new DogBean("Dog", "Dog 2", 4, "green", 13);
            DogBean d3 = new DogBean("Dog", "Dog 3", 4, "red", 13);


            //make test on dogs


            //step 3 make set of objects

            TreeMap<String, DogBean> mt = new TreeMap<>();
            mt.put("Rex", d1);
            mt.put("Dog", d2);

            assertEquals(true, mt.containsKey("Rex"));

            //mt.remove("Rex");
            //assertEquals(true, mt.containsKey("Rex"));



    }

}