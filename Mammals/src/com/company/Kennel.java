package com.company;
import com.company.DogBean;

public class Kennel {


    private DogBean[] buildDogs(){

        DogBean[] dogBean = new DogBean[5];

        dogBean[0] = new DogBean("Labrador Retriever",  "Bella", 3, "yellow", 22.4);
        dogBean[1] = new DogBean("German Shepherd", "Lucy", 2, "black", 65);
        dogBean[2] = new DogBean("Golden Retriever",  "Daisy", 4, "Golden", 24);
        dogBean[3] = new DogBean("Bulldog",  "Luna", 4, "red", 40);
        dogBean[4] = new DogBean("Beagle",  "Lola", 4, "brown and white", 13);

        return dogBean;

    }


    private void displayDogs(DogBean[] dogBeans){

        for(int i = 0; i < 5; i++){
            System.out.println(dogBeans[i].toString());
        }

    }


    public static void main(String[] args){

        Kennel kennel = new Kennel();
        DogBean[] allDogs = kennel.buildDogs();
        kennel.displayDogs(allDogs);
        
    }
}
