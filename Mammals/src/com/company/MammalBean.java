package com.company;

public class MammalBean {

    private int legCount;
    private String color;
    private double height;



     MammalBean(int l, String c, double h ){

        //set the default values
        legCount = l;
        color = c;
        height = h;

    }

    public void setLegCount(int l){
        legCount = l;
    }

    public int getLegCount(){
        return legCount;
    }

    private void setColor(String c){
        color = c;
    }

    public String getColor(){
        return color;
    }

    private void setHeight(double h){
        height = h;
    }


    public double getHeight(){
        return height;
    }




    @Override
    public String toString(){

        return "legCount=" + this.legCount + " color=" + this.color + " height=" + this.height;

    }
}
