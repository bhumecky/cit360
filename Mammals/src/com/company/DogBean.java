package com.company;

public class DogBean extends MammalBean {

    private String breed;
    private String name;


     DogBean(String b, String n, int l, String c, double h ){
        super(l, c, h);

        breed = b;
        name = n;

    }

    public String getBreed() {
        return breed;
    }

    public String getName() {
        return name;
    }



    private void setBreed(String breed) {
        this.breed = breed;
    }

    private void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {


        return super.toString() + " breed=" + this.breed + " name=" + this.name;

    }
}
